package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.dto.IAbstractModelDtoRepository;
import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractModelDtoRepository<M extends AbstractModelDTO> implements IAbstractModelDtoRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractModelDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == DateComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public abstract void clear();

    @Nullable
    @Override
    public abstract List<M> findAll();

    @Nullable
    @Override
    public abstract M findOneById(@NotNull String id);

    @Override
    public abstract void removeOneById(@NotNull String id);

    @Override
    public abstract int getSize();

}
