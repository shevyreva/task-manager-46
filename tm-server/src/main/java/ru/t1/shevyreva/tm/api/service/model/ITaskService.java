package ru.t1.shevyreva.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {


    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    List<Task> findAll(@Nullable final String userId, @Nullable final TaskSort sort);

    @NotNull
    int getSize(@Nullable final String userId);

    @SneakyThrows
    void removeAll(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    Task removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    Task removeOne(@Nullable final String userId, @Nullable final Task task);

    @NotNull
    @SneakyThrows
    Task findOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    @SneakyThrows
    Collection<Task> set(@NotNull final Collection<Task> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<Task> add(@NotNull final Collection<Task> models);

    @SneakyThrows
    List<Task> findAll();

    boolean existsById(@NotNull final String user_id, @NotNull final String id);

}
