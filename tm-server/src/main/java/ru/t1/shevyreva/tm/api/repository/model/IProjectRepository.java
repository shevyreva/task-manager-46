package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IAbstractUserOwnedModelRepository<Project> {

    @Nullable
    Project findOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByIndex(@NotNull Integer index);

    boolean existsByIdForUser(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull final String id);

    List<Project> findAll(@Nullable final Comparator<Project> comparator);

    List<Project> findAllForUser(@NotNull final String userId, @Nullable final Comparator<Project> comparator);

    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String id);

}
