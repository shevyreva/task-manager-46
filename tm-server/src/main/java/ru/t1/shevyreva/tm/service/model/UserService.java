package ru.t1.shevyreva.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.IUserRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.model.IUserService;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsEmailException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.repository.model.UserRepository;
import ru.t1.shevyreva.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class UserService implements IUserService {

    private final IConnectionService connectionService;

    private final IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);
        user.setEmail(email);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(role);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByLogin(login);
            userRepository.removeOne(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findByEmail(email);
            userRepository.removeOne(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = userRepository.findOneById(id);
            userRepository.removeOne(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = findOneById(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull final User user = findOneById(id);
            user.setPasswordHash(HashUtil.salt(password, propertyService));
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return (userRepository.findByLogin(login) != null);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return (userRepository.findByEmail(email) != null);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User lockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull User user = findByLogin(login);
            user.setLocked(true);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User unlockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @NotNull User user = findByLogin(login);
            user.setLocked(false);
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findOneById(id);
            if (user == null) throw new ModelNotFoundException();
            return user;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<User> add(@NotNull final Collection<User> models) {
        if (models == null) throw new UserNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            for (@NotNull User user : models) {
                userRepository.add(user);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<User> set(@NotNull final Collection<User> models) {
        @Nullable final Collection<User> entities;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
            removeAll();
            entityManager.getTransaction().begin();
            entities = add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entities;
    }

}
