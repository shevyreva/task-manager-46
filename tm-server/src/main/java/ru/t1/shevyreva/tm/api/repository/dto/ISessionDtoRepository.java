package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDtoRepository extends IAbstractUserOwnedModelDtoRepository<SessionDTO> {

    void removeOne(@NotNull final SessionDTO session);

    @Override
    public void clearForUser(@NotNull String userId);

    @Override
    public void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public List<SessionDTO> findAllForUser(@NotNull String userId);

    @Override
    public SessionDTO findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public int getSizeForUser(@NotNull String userId);

    @Override
    public void clear();

    @Override
    public @Nullable List<SessionDTO> findAll();

    @Override
    public @Nullable SessionDTO findOneById(@NotNull String id);

    @Override
    public void removeOneById(@NotNull String id);

    @Override
    public int getSize();

}
