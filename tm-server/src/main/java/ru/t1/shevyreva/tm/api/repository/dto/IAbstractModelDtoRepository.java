package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IAbstractModelDtoRepository<M extends AbstractModelDTO> {

    void add(@NotNull final M model);

    void update(@NotNull final M model);

    abstract void clear();

    abstract List<M> findAll();

    abstract M findOneById(@NotNull final String id);

    abstract void removeOneById(@NotNull final String id);

    abstract int getSize();

}
