package ru.t1.shevyreva.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class TaskDtoRepository extends AbstractUserOwnedModelDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearForUser(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO")
                .executeUpdate();
    }

    @Override
    public List<TaskDTO> findAllForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class).getResultList();
    }

    @Override
    public List<TaskDTO> findAll(@Nullable final Comparator<TaskDTO> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM TaskDTO e ORDER BY e." + sortType, TaskDTO.class).getResultList();
    }

    @Override
    public List<TaskDTO> findAllForUser(@NotNull final String userId, @Nullable final Comparator<TaskDTO> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId ORDER BY e." + sortType, TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public TaskDTO findOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public TaskDTO findOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public TaskDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM TaskDTO e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public boolean existsByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findOneByIdForUser(userId, id) != null;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT e FROM TaskDTO e WHERE e.projectId = :projectId AND e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
