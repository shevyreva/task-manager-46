package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "3333";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "423423";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private final String SESSION_KEY = "session.key";

    @NotNull
    private final String SESSION_TIMEOUT = "session.timeout";

    @NotNull
    private final String DATABASE_USERNAME_PROPERTY = "database.username";

    @NotNull
    private final String DATABASE_USERNAME_DEFAULT_VALUE = "postgres";

    @NotNull
    private final String DATABASE_PASSWORD_PROPERTY = "database.password";

    @NotNull
    private final String DATABASE_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private final String DATABASE_URL_PROPERTY = "database.url";

    @NotNull
    private final String DATABASE_URL_DEFAULT_VALUE = "dbc:postgresql://127.0.0.1:5432/task-manager";

    @NotNull
    private final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQL9Dialect";

    @NotNull
    private final String HBM2DDL_AUTO = "database.hmb2ddl_auto";

    @NotNull
    private final String HBM2DDL_AUTO_DEFAULT = "update";

    @NotNull
    private final String SHOW_SQL = "database.show_sql";

    @NotNull
    private final String USE_SECOND_LEVEL_CACHE = "database.cache.use_second_level_cache";

    @NotNull
    private final String USE_QUERY_CACHE = "database.cache.use_query_cache";

    @NotNull
    private final String USE_MINIMAL_PUTS = "database.cache.use_minimal_puts";

    @NotNull
    private final String CACHE_REGION_PREFIX = "database.cache.region_prefix";

    @NotNull
    private final String CACHE_REGION_FACTORY = "database.cache.region_factory_class";

    @NotNull
    private final String CACHE_PROVIDER_CONFIG = "database.cache.provider_configuration_file_resource_path";

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return "1.31.0";
        //return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return "Shevyreva Liya";
        //return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return "lmaximova@gmail.com";
        //return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_PROPERTY, DATABASE_PASSWORD_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_PROPERTY, DATABASE_URL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_PROPERTY, DATABASE_USERNAME_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getHBM2DDL() {
        return getStringValue(HBM2DDL_AUTO, HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getShowSQL() {
        return getStringValue(SHOW_SQL);
    }

    @NotNull
    public String getUseSecondLevelCache() {
        return getStringValue(USE_SECOND_LEVEL_CACHE);
    }

    @NotNull
    public String getUseQueryCache() {
        return getStringValue(USE_QUERY_CACHE);
    }

    @NotNull
    public String getUseMinimalPuts() {
        return getStringValue(USE_MINIMAL_PUTS);
    }

    @NotNull
    public String getCacheRegionPrefix() {
        return getStringValue(CACHE_REGION_PREFIX);
    }

    @NotNull
    public String getCacheRegionFactory() {
        return getStringValue(CACHE_REGION_FACTORY);
    }

    @NotNull
    public String getCacheProviderConfig() {
        return getStringValue(CACHE_PROVIDER_CONFIG);
    }
}
