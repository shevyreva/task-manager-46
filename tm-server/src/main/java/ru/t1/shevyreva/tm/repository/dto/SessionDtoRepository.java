package ru.t1.shevyreva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDtoRepository extends AbstractUserOwnedModelDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.userId  = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<SessionDTO> findAllForUser(@NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId  = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public SessionDTO findOneByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.id  = :id AND e.userId  = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public int getSizeForUser(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM SessionDTO e WHERE e.userId  = :userId", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO")
                .executeUpdate();
    }

    @Override
    public @Nullable List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    @Override
    public @Nullable SessionDTO findOneById(@NotNull String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.createQuery("DELETE FROM SessionDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOne(@NotNull final SessionDTO session) {
        entityManager.remove(session);
    }


    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM SessionDTO e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

}
