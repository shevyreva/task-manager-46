package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(@Nullable List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
