package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

@Setter
@Getter
@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@NotNull UserDTO user) {
        super(user);
    }

}
