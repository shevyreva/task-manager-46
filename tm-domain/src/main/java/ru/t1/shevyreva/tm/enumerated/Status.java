package ru.t1.shevyreva.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {

    NON_STARTED("Non Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    @Nullable
    private final String displayName;

    Status(@Nullable final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

}
