package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

@Setter
@Getter
@NoArgsConstructor
public class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
