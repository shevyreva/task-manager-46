package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataLoadBase64Request extends AbstractUserRequest {

    public DataLoadBase64Request(@Nullable String token) {
        super(token);
    }

}
