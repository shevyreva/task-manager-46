package ru.t1.shevyreva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String userId;

    public UserRemoveRequest(@Nullable String token, @Nullable String login, @Nullable String email, @Nullable String userId) {
        super(token);
        this.login = login;
        this.email = email;
        this.userId = userId;
    }

}
