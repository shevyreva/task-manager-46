package ru.t1.shevyreva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.enumerated.ProjectDtoSort;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectDtoSort sort;

    public ProjectListRequest(@Nullable String token, @Nullable ProjectDtoSort sort) {
        super(token);
        this.sort = sort;
    }

}
