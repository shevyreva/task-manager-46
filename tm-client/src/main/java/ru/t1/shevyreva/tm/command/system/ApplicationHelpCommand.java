package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show about program.";

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
