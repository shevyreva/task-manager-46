package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Remove project by Id.";

    @NotNull
    private final String NAME = "project-remove-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken(), id);
        getProjectEndpoint().removeProjectById(request);
    }

}
